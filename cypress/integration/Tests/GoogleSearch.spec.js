import {searchPage} from "../../fixtures/Pages/googlesearchPage"
import {basePage} from "../BasePage/BaseTestPage"



describe("Testing Google Search Functionality",()=>{
    beforeEach("Accessing the url", function(){
        cy.visit("https://www.google.com/");
        cy.fixture('example').as('testContext');
 
    });

    it("Verify google saerch textbox, Google search and I' m feeling luck button is displayed" ,function(){
        
        searchPage.verifySearchTextBoxisDisplayed();
        searchPage.verifyGoogleSearchButtonIsDisplayed();
        searchPage.verifyFeelingLuckyButtonisDisplayed();

    });

    it("Perform google search and verify the search results are as expected", ()=>{
        
        cy.get("@testContext").then((testContext)=>{
            searchPage.performSearchWithValidKeyword(testContext.searchText);
            searchPage.verifySearchResults(testContext.searchText);
        });
        
    });
});


