import {searchPage} from "../../fixtures/Pages/googlesearchPage"
import {basePage} from "../BasePage/BaseTestPage"



describe("Testing Google Search Functionality",()=>{
    beforeEach("Accessing the url", function(){
        cy.visit("https://www.google.com/");
        cy.fixture('example').as('testContext');
 
    });

    it("Perform google search and verify the search results are as expected", ()=>{
        
        cy.get("@testContext").then((testContext)=>{
            searchPage.performSearchWithValidKeyword(testContext.invalidText);
            searchPage.verifySearchResultsNotFoundValidation();
        });
        
    });

});