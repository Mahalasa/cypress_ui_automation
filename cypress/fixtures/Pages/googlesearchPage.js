///<reference types="Cypress" />

export class GoogleSearchPage{

    verifyGoogleSearchButtonIsDisplayed(){
        cy.contains("Google Search");
    }

    verifyFeelingLuckyButtonisDisplayed(){
        cy.contains("I'm Feeling Lucky");
    }

    verifySearchTextBoxisDisplayed(){
        cy.get('.gLFyf').then($searchTextbox=>{
            if($searchTextbox.is(':visible')){
                 console.log("Search textbox is displayed");

            };
        });
    }

    performSearchWithValidKeyword(searchKeyword){
        cy.get('.gLFyf').type(searchKeyword);
        if(!cy.contains("I'm Feeling Lucky")){
            cy.get('.erkvQe > :nth-child(1)').then($autopopulate=>{
                cy.get('.erkvQe > :nth-child(1)').click();
            });
        }else{
            cy.get('.FPdoLc > center > .gNO89b').click();
         }
        
    }

    verifySearchResults(searchKeyword){
        cy.get('[href="https://www.anoushkashankar.com/"] > .LC20lb > span').contains(searchKeyword);
     }

    verifySearchResultsNotFoundValidation(searchKeyword){
       
        cy.get('.card-section').contains("Your search");
        console.log("Result not found message is displayed");
    }

    verifyClearTextandEnterNewSearchKeyword(){
        cy.get('.lBbtTb > svg').click();
        //click on Google logo link
        cy.get('#logo > img').click();
    }

    verifyAutoPopulateListBoxIsDisplayed(){
        cy.get('.erkvQe').then($autopopulate=>{
            if($autopopulate.is(":visible")){
                console.log("Auto populate listbox is displayed");
            }
        })
    }
}

export const searchPage = new GoogleSearchPage();